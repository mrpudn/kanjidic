# frozen_string_literal: true

require 'fileutils'
require 'net/http'
require 'stringio'
require 'zlib'
require 'json'

require 'nokogiri'

KANJIDIC_URL = 'https://www.edrdg.org/kanjidic/kanjidic2.xml.gz'

def update
  download
  build
  puts 'Done!'
end

def download
  return if File.directory?('tmp')

  puts 'Downloading kanjidic.xml ...'

  FileUtils.mkdir_p('tmp')

  response = Net::HTTP.get_response(URI(KANJIDIC_URL))
  File.write('tmp/kanjidic.xml', gz_read(response.body))
end

def gz_read(str)
  Zlib::GzipReader.new(StringIO.new(str)).read
end

def build
  return if File.directory?('data')

  puts 'Building kanjidic ...'

  FileUtils.mkdir_p('data/kanji')
  FileUtils.mkdir_p('data/indexes')

  doc = File.open('tmp/kanjidic.xml') { |f| Nokogiri::XML(f) }

  header = get_header(doc.xpath('//header').first)
  save_header(header)

  characters = get_characters(doc.xpath('//character'))
  save_characters(characters)

  indexes = get_indexes(characters)
  save_indexes(indexes)
end

def get_header(element)
  {
    'file_version' => element.xpath('file_version').first.content,
    'database_version' => element.xpath('database_version').first.content,
    'date_of_creation' => element.xpath('date_of_creation').first.content
  }
end

def save_header(header)
  File.write('data/header.json', header.to_json)
end

def get_characters(elements)
  elements.map { |element| get_character(element) }
end

def get_character(element)
  character = {}

  character['literal'] = element.xpath('literal').first.content

  character['codepoint'] = {}
  element.xpath('codepoint/cp_value').each do |codepoint|
    type = codepoint.attributes['cp_type'].value
    value = codepoint.content
    character['codepoint'][type] = value
  end

  character['radical'] = {'ref' => {}}
  element.xpath('radical/rad_value').each do |radical|
    type = radical.attributes['rad_type'].value
    value = radical.content
    character['radical']['ref'][type] = value
  end

  grade = element.xpath('misc/grade').first
  character['grade'] =
    unless grade.nil?
      grade.content.to_i
    else
      nil
    end

  character['strokes'] = element.xpath('misc/stroke_count').first.content.to_i

  character['variants'] = {}
  element.xpath('misc/variant').each do |variant|
    type = variant.attributes['var_type'].value
    value = variant.content
    (character['variants'][type] ||= []) << value
  end

  freq = element.xpath('misc/freq').first
  character['frequency'] =
    unless freq.nil?
      freq.content.to_i
    else
      nil
    end

  rad_name = element.xpath('misc/rad_name').first
  unless rad_name.nil?
    character['radical']['self'] = true
    character['radical']['name'] = rad_name.content
  else
    character['radical']['self'] = false
    character['radical']['name'] = nil
  end

  jlpt = element.xpath('misc/jlpt').first
  character['jlpt'] =
    unless jlpt.nil?
      jlpt.content.to_i
    else
      nil
    end

  character['references'] = {}
  element.xpath('dic_number/dic_ref').each do |ref|
    type = ref.attributes['dr_type'].value
    value = ref.content.to_i
    character['references'][type] = value
  end

  character['entries'] = []
  element.xpath('reading_meaning/rmgroup').each do |group|
    readings = {}
    group.xpath('reading').each do |reading|
      type = reading.attributes['r_type'].value
      value = reading.content
      (readings[type] ||= []) << value
    end

    meanings = {}
    group.xpath('meaning').each do |meaning|
      m_lang = meaning.attributes['m_lang']
      type =
        unless m_lang.nil?
          m_lang.value
        else
          'en'
        end
      value = meaning.content
      (meanings[type] ||= []) << value
    end

    character['entries'] << {
      'reading' => readings,
      'meaning' => meanings
    }
  end

  character['nanori'] = element.xpath('reading_meaning/nanori')
    .map { |e| e.content }

  character['query'] = {}
  element.xpath('query_code/q_code').each do |code|
    type = code.attributes['qc_type'].value
    value = code.content
    character['query'][type] = value
  end

  character
end

def save_characters(characters)
  characters.each do |character|
    File.open("data/kanji/#{character['literal']}.json", 'w') do |f|
      f.puts character.to_json
    end
  end
end

def get_indexes(characters)
  {
    jlpt: index_by_jlpt(characters),
    jouyou: index_by_jouyou(characters),
    jinmeiyou: index_by_jinmeiyou(characters),
    radical: index_by_radical(characters),
    strokes: index_by_strokes(characters)
  }
end

def index_by_jlpt(characters)
  characters
    .select { |c| c['jlpt'] }
    .map { |c| [c['jlpt'], c['literal']] }
    .sort_by { |r| [-r[0], r[1]] }
end

def index_by_jouyou(characters)
  characters
    .select { |c| c['grade'] && c['grade'] < 9 }
    .map { |c| [c['grade'], c['literal']] }
    .sort
end

def index_by_jinmeiyou(characters)
  characters
    .select { |c| c['grade'] && c['grade'] > 8 }
    .map { |c| [c['grade'], c['literal']] }
    .sort
end

def index_by_radical(characters)
  characters
    .select { |c| c['radical']['ref']['classical'] }
    .map { |c| [c['radical']['ref']['classical'].to_i, c['literal']] }
    .sort
end

def index_by_strokes(characters)
  characters
    .select { |c| c['strokes'] }
    .map { |c| [c['strokes'], c['literal']] }
    .sort
end

def save_indexes(indexes)
  indexes.each do |key, index|
    File.open("data/indexes/#{key}.json", 'w') do |f|
      f.puts index.to_json
    end
  end
end

update
