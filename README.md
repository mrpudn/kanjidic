# kanjidic

A redistribution of the `KANJIDIC2` kanji dictionary.

## Updating

Delete any existing data:
```sh
$ rm -rf tmp data
```

Run `update.rb`:
```sh
$ ruby update.rb
```

## License

This project redistributes the KANJIDIC2 dictionary file. This file is the
property of the Electronic Dictionary Research and Development Group (EDRDG),
and is used in conformance with the Group's license.

The remainder of this project is licensed under the MIT license.

See `LICENSE` for more information.
